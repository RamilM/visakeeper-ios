//
//  VisaCell.swift
//  VisaKeeper
//
//  Created by Ramil Minibaev on 20/01/2018.
//  Copyright © 2018 Ramil Minibaev. All rights reserved.
//

import Foundation
import UIKit
import CountryPickerView
import SwipeCellKit

class VisaCell: SwipeTableViewCell {
    
    @IBOutlet var flagLabel: UILabel!
    @IBOutlet var countryNameLabel: UILabel!
    @IBOutlet var datesLabel: UILabel!
    
    var countryISO: String = ""
    var startDate: Date = Date.init()
    var expiringDate: Date = Date.init()
    
    private let countryPickerView = CountryPickerView()
    private let dateFormatter = DateFormatter()


    func show(visa: Visa){
        self.countryISO = visa.countryISO!
        self.startDate = visa.startDate!
        self.expiringDate = visa.endDate!
        
        let country = countryPickerView.getCountryByCode(visa.countryISO!)!
        flagLabel.text=flagByCode(country: country.code.uppercased())
        self.countryNameLabel.text=country.name
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.locale = Locale.current
        self.datesLabel.text=dateFormatter.string(from: visa.startDate!)+" - "+dateFormatter.string(from: visa.endDate!)
        
        let daysLeft = NSCalendar.current.dateComponents([.day], from: Date.init(), to: expiringDate).day!
        
        if(daysLeft<14){
            self.datesLabel.text="Expires in \(daysLeft) day(s)"
            self.datesLabel.textColor = UIColor.red
            if(daysLeft<0){
                self.datesLabel.text="Expired"
                self.datesLabel.textColor = UIColor.red
            }
        }
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func flagByCode(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
}
