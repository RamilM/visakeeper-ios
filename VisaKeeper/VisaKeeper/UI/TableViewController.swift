//
//  ViewController.swift
//  VisaKeeper
//
//  Created by Ramil Minibaev on 16/01/2018.
//  Copyright © 2018 Ramil Minibaev. All rights reserved.
//

import UIKit
import CountryPickerView
import SwipeCellKit

class TableViewController: UITableViewController, SwipeTableViewCellDelegate {
    
    let visaModel = VisaModel.instance
    var visaList : [Visa] = []
    var indexForEdit : IndexPath = IndexPath(row: -1, section: -1)
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(indexForEdit.row != -1){
        let controller = segue.destination as! EditViewController
        let cell = tableView.cellForRow(at: indexForEdit) as! VisaCell
        controller.windedCountryCode=cell.countryISO
        controller.windedStartDate=cell.startDate
        controller.windedEndDate=cell.expiringDate
        }
        else{return}
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .left {
            return []
        } else {
            let editAction = SwipeAction(style: .default, title: "Edit"){action, indexPath in
                self.indexForEdit=indexPath
                self.visaModel.deleteVisaByNumber(numberInFetch: self.indexForEdit.row+1)
                self.performSegue(withIdentifier: "AddOrEdit", sender: self)
            }
            editAction.title="Edit"
            editAction.backgroundColor=#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            editAction.textColor=#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            
            let deleteAction = SwipeAction(style: .destructive, title: "Delete"){action, indexPath in
                self.visaModel.deleteVisaByNumber(numberInFetch: indexPath.row+1)
                self.refreshData()
                tableView.reloadData()
            }
            deleteAction.title="Delete"
            deleteAction.backgroundColor=#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            deleteAction.textColor=#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            
            return [deleteAction,editAction]
        }
    }
    
    
    func refreshData(){
        visaList=visaModel.getVisas()
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .border
        return options
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //fetch core data entities
        visaList=visaModel.getVisas()
        //delete footer
        tableView.tableFooterView = UIView()
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visaList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let visa = visaList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaCell") as! VisaCell
        cell.show(visa: visa)
        cell.delegate = self
        return cell
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

