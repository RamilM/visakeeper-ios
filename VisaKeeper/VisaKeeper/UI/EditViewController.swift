//
//  EditViewController.swift
//  VisaKeeper
//
//  Created by Ramil Minibaev on 28/01/2018.
//  Copyright © 2018 Ramil Minibaev. All rights reserved.
//

import Foundation
import UIKit
import CountryPickerView
import CoreData
class EditViewController : UIViewController{
    
    @IBOutlet weak var countryPicker: CountryPickerView!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    var windedCountryCode : String = "US"
    var windedStartDate : Date = Date.init()
    var windedEndDate : Date = Date.init()
    
    let managedContext = VisaModel.instance.managedContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryPicker.setCountryByCode(windedCountryCode)
        startDatePicker.setDate(windedStartDate, animated: false)
        endDatePicker.setDate(windedEndDate, animated: false)
        countryPicker.showPhoneCodeInView=false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let visaEntity = NSEntityDescription.entity(forEntityName: "Visa", in: managedContext)!
        let visa = NSManagedObject(entity: visaEntity, insertInto: managedContext)
        visa.setValue(countryPicker.selectedCountry.code, forKey: "countryISO")
        visa.setValue(startDatePicker.date, forKey: "startDate")
        visa.setValue(endDatePicker.date, forKey: "endDate")
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if(endDatePicker.date>startDatePicker.date){
            return true
        } else {
            let alert = UIAlertController(title: "Error", message: "Expiration date must be later start date", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
    }
    
}
