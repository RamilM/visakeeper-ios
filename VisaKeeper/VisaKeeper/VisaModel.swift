//
//  DataModel.swift
//  VisaKeeper
//
//  Created by Ramil Minibaev on 17/01/2018.
//  Copyright © 2018 Ramil Minibaev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class VisaModel {
    let appDelegate : AppDelegate?
    let managedContext : NSManagedObjectContext
    static let instance = VisaModel()

    init() {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = (appDelegate?.persistentContainer.viewContext)!
    }
    
    
    
    func getVisas() -> [Visa] {
        let visasList : [Visa]
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Visa")
        do {
            visasList = try managedContext.fetch(request).sorted{($0 as! Visa).endDate! < ($1  as! Visa).endDate!} as! [Visa]
        } catch {
            print("fetch error")
            visasList = []
        }
        return visasList
    }
    
    func deleteVisaByNumber(numberInFetch: Int){
        var i : Int = 0;
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Visa")
        if let result = try? managedContext.fetch(fetchRequest).sorted{($0 as! Visa).endDate! < ($1  as! Visa).endDate!} {
            for object in result {
                i+=1
                if (i == numberInFetch){
                    managedContext.delete(object as! NSManagedObject)
                }
            }
        }
        do {
            try managedContext.save()
        } catch {
            print("saving error")
        }
    }
   
}
